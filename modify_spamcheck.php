<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		Ruud Eisinga, LEPTON project
 *	@copyright		2012-2020 Ruud Eisinga, LEPTON project
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if (defined("LEPTON_PATH")) {
	include(LEPTON_PATH."/framework/class.secure.php");
} else {
	$root = "../";
	$level = 1;
	while (($level < 10) && (!file_exists($root."/framework/class.secure.php"))) {
		$root .= "../";
		$level += 1;
	}
	if (file_exists($root."/framework/class.secure.php")) {
		include($root."/framework/class.secure.php");
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER["SCRIPT_NAME"]), E_USER_ERROR);
	}
}
// end include class.secure.php 

// Include admin wrapper script
require(LEPTON_PATH."/modules/admin.php");

// include module class
$oQUICKFORM = quickform::getInstance();

// set validation
$oREQUEST = LEPTON_request::getInstance();

// handle on incoming action
$action = "edit";
if ( isset( $_POST["action"] ))
{
	$input_fields = array (
		 'action'			=> array ('type' => 'string_clean', 'default' => "edit")
	);	
	$valid_fields = $oREQUEST->testPostValues($input_fields);
	$action = strtolower( $valid_fields["action"] );
};

// process action
$error = "";
$show = true;
$pages = array();
$valid_fields = array();

switch( $action )
{
	case "save":
		// validate input
		$input_fields = array(
			 'use_honeypot'		=> array ('type' => 'integer+', 'default' => 0, 'min' => 0, 'max' => 1, 'use' => 'min' )
			,'spam_logging'		=> array ('type' => 'integer+', 'default' => 0, 'min' => 0, 'max' => 1, 'use' => 'min' )
			,'spam_checktime'	=> array ('type' => 'integer+', 'default' => 0, 'min' => 0, 'max' => 60, 'use' => 'min' )
			,'spam_honeypot'	=> array ('type' => 'string_clean', 'default' => "" )
			,'spam_failpage'	=> array ('type' => 'integer', 'default' => 0, 'min' => -1, 'max' => 32000 )
		);
		$valid_fields = $oREQUEST->testPostValues($input_fields);

		// special validation
		if ( $valid_fields["spam_failpage"] > 0 )
		{
			// get page links
			$oQUICKFORM->build_pagelist( 0, $page_id );	// set global $pages
			if ( array_key_exists( (int)$valid_fields["spam_failpage"], $pages ) == false ) 
			{
				$error = "ERROR_SPAMFAIL";
				break;
			}
		}

		// write settings to DB
		$update_when_modified = true; 
		$database->build_and_execute(
			  "update"
			, TABLE_PREFIX."mod_quickform"
			, $valid_fields
			, "`section_id` = " . $section_id
		);

		$show = false;
		$admin->print_success($TEXT["SUCCESS"], ADMIN_URL."/pages/modify.php?page_id=".(int)$page_id);
		break;

	default:
		// get settings for section
		$valid_fields = $oQUICKFORM->get_settings( $section_id );
		break;
};

// show backend template
if ( $show == true )
{
	// get language setup
	$MOD_QUICKFORM = $oQUICKFORM->language;

	// prepare url's used
	$urls = Array(
		  "action"	=> ADMIN_URL."/pages/modify.php?page_id=".$page_id."&section_id=".$section_id
		, "help"	=> "https://doc.lepton-cms.org/docu/english/tutorials/doc-quickform.php"
	);

	// build dropdown list for success page
	if ( count( $pages ) == 0 )
	{
		$oQUICKFORM->build_pagelist( 0, $page_id );
	}

	// set template values
	$page_values = array(
			 "form_action"		=> $_SERVER["SCRIPT_NAME"]
			,"oQUICKFORM"		=> $oQUICKFORM
			,"data"				=> $valid_fields
			,"urls"				=> $urls
			,"leptoken"			=> get_leptoken()
			,"page_id"			=> $page_id
			,"section_id"		=> $section_id
			,"MESSAGE_CLASS"	=> "hidden"
			,"STATUSMESSAGE"	=> ""
			,"pagelist"			=> $pages
		);

	// set error
	if ( $error != "" )
	{
		$page_values["MESSAGE_CLASS"]	= "";
		$page_values["STATUSMESSAGE"]	= $MOD_QUICKFORM[ $error ];
	}

	// show screen
	$oTWIG = lib_twig_box::getInstance();	
	$oTWIG->registerModule( "quickform" );
	echo $oTWIG->render(
		 "@quickform/modify_spamcheck.lte"
		,$page_values
	);
}

// Print admin footer
$admin->print_footer();

?>
