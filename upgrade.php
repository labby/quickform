<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		Ruud Eisinga, LEPTON project
 *	@copyright		2012-2020 Ruud Eisinga, LEPTON project
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$root = "../";
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= "../";
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

// save original tables
LEPTON_handle::create_sik_table('mod_quickform');
LEPTON_handle::create_sik_table('mod_quickform_data');


$database = LEPTON_database::getInstance();


// -------------------------------------------
// add database fields: mod_quickform_data
$fields = array();
$database->describe_table(
	 TABLE_PREFIX."mod_quickform_data"
	,$fields
	,1		// LEPTON_database::DESCRIBE_ASSOC
);

if ( array_key_exists('msg_group', $fields) == false )
{
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_quickform_data` ADD `msg_group` VARCHAR(32) NOT NULL DEFAULT 'INBOX' AFTER `section_id`; ");
	$database->simple_query("UPDATE `".TABLE_PREFIX."mod_quickform_data` SET `msg_group` = 'INBOX'; ");
}


// -------------------------------------------
// add database fields: mod_quickform
$fields = array();
$database->describe_table(
	 TABLE_PREFIX."mod_quickform"
	,$fields
	,1		// LEPTON_database::DESCRIBE_ASSOC
);
if ( array_key_exists('usenbritems', $fields) == false )
{
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_quickform` ADD `usenbritems` INT NOT NULL DEFAULT '50' AFTER `successpage`; ");
	$database->simple_query("UPDATE `".TABLE_PREFIX."mod_quickform` SET `usenbritems` = 50; ");
}
if ( array_key_exists('useview', $fields) == false )
{
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_quickform` ADD `useview` VARCHAR(12) NOT NULL DEFAULT 'TABLE' AFTER `usenbritems`; ");
	$database->simple_query("UPDATE `".TABLE_PREFIX."mod_quickform` SET `useview` = 'CLASSIC'; ");
}
if ( array_key_exists('use_honeypot', $fields) == false )
{
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_quickform` ADD `use_honeypot` tinyint(1) NULL DEFAULT '0' AFTER `useview`; ");
	$database->simple_query("UPDATE `".TABLE_PREFIX."mod_quickform` SET `use_honeypot` = 0; ");
}
if ( array_key_exists('spam_logging', $fields) == false )
{
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_quickform` ADD `spam_logging` tinyint(1) NULL DEFAULT '0' AFTER `use_honeypot`; ");
	$database->simple_query("UPDATE `".TABLE_PREFIX."mod_quickform` SET `spam_logging` = 0; ");
}
if ( array_key_exists('spam_checktime', $fields) == false )
{
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_quickform` ADD `spam_checktime` INT NULL DEFAULT '0' AFTER `spam_logging`; ");
	$database->simple_query("UPDATE `".TABLE_PREFIX."mod_quickform` SET `spam_checktime` = 0; ");
}
if ( array_key_exists('spam_honeypot', $fields) == false )
{
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_quickform` ADD `spam_honeypot` VARCHAR(256) NULL DEFAULT '' AFTER `spam_checktime`; ");
}
if ( array_key_exists('spam_failpage', $fields) == false )
{
	$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_quickform` ADD `spam_failpage` INT NULL DEFAULT '0' AFTER `spam_honeypot`; ");
	$database->simple_query("UPDATE `".TABLE_PREFIX."mod_quickform` SET `spam_failpage` = 0; ");
}

// -------------------------------------------
// update template form
// assuming that there are not so many pages with a contact form ...
if ( array_key_exists('useview', $fields) == false )
{
	$rows = array();
	$database->execute_query( "SELECT `section_id`, `template` from `".TABLE_PREFIX."mod_quickform`;", $rows );
	if ( count( $rows) > 0 )
	{
		foreach ( $rows as $row)
		{
			$pageID = $database->get_one("SELECT `page_id` FROM `".TABLE_PREFIX."sections` WHERE `section_id` = ".(int)$row[ "section_id"] . ";" );
			$langCD = $database->get_one("SELECT `language` FROM `".TABLE_PREFIX."pages` WHERE `page_id`=".(int)$pageID . ";" );

			switch( strtolower($langCD) )
			{
				case "de":
					$templates = array(
						 'contactform.lte' 							=> 'qf_kontakt.lte'
						,'contactform (HTML5).lte' 					=> 'qf_kontakt.lte'
						,'contactform_(HTML5).lte' 					=> 'qf_kontakt.lte'
						,'mini_contactform.lte'						=> 'qf_kontakt_mini_honeypot.lte'
						,'mini_contactform (HTML5).lte'				=> 'qf_kontakt_mini_honeypot.lte'
						,'mini_contactform_(HTML5).lte'				=> 'qf_kontakt_mini_honeypot.lte'
						,'mini_contactform_semantic.lte'			=> 'qf_kontakt_mini_semantic.lte'
						,'mini_contactform_semantic (HTML5).lte'	=> 'qf_kontakt_mini_semantic.lte'
						,'mini_contactform_semantic_(HTML5).lte'	=> 'qf_kontakt_mini_semantic.lte'
					);
					break;
				case "nl":
					$templates = array(
						 'bel_me_terug.lte'							=> 'qf_bel_me_terug.lte'
						,'bel_me_terug (HTML5).lte'					=> 'qf_bel_me_terug.lte'
						,'bel_me_terug_(HTML5).lte'					=> 'qf_bel_me_terug.lte'
						,'contactformulier.lte'						=> 'qf_contact.lte'
						,'contactformulier (HTML5).lte'				=> 'qf_contact.lte'
						,'contactformulier_(HTML5).lte'				=> 'qf_contact.lte'
						,'mini_contactformulier.lte'				=> 'qf_contact_mini.lte'
						,'mini_contactformulier (HTML5).lte'		=> 'qf_contact_mini.lte'
						,'mini_contactformulier_(HTML5).lte'		=> 'qf_contact_mini.lte'
						,'uitgebreid_contactformulier.lte'			=> 'qf_contact_uitgebreid.lte'
						,'uitgebreid_contactformulier (HTML5).lte'	=> 'qf_contact_uitgebreid.lte'
						,'uitgebreid_contactformulier_(HTML5).lte'	=> 'qf_contact_uitgebreid.lte'
					);
					break;
				default:
					$templates = array(
						 'callme.lte'								=> 'qf_callme.lte'
						,'callme (HTML5).lte'						=> 'qf_callme.lte'
						,'callme_(HTML5).lte'						=> 'qf_callme.lte'
						,'contactform.lte' 							=> 'qf_contact.lte'
						,'contactform (HTML5).lte' 					=> 'qf_contact.lte'
						,'contactform_(HTML5).lte' 					=> 'qf_contact.lte'
						,'mini_contactform.lte'						=> 'qf_contact_mini_honeypot.lte'
						,'mini_contactform (HTML5).lte'				=> 'qf_contact_mini_honeypot.lte'
						,'mini_contactform_(HTML5).lte'				=> 'qf_contact_mini_honeypot.lte'
						,'full_contactform.lte'						=> 'qf_contact_full.lte'
						,'full_contactform (HTML5).lte'				=> 'qf_contact_full.lte'
						,'full_contactform_(HTML5).lte'				=> 'qf_contact_full.lte'
					);
					break;
			}

			// update with new template name
			if ( array_key_exists( $row[ "template"], $templates ) === true )
			{
				$database->simple_query("UPDATE `".TABLE_PREFIX."mod_quickform`"
										. " SET `template` = '" . $templates[$row[ "template"]] . "'"
										. " WHERE `section_id` = " . (int)$row[ "section_id"] . ";" );
			}
		}
	}
}


// -------------------------------------------
// delete old template files
$file_names = array();
if ( false == file_exists( LEPTON_PATH . '/modules/quickform/templates/backend/qf_email.lte' ) )
{
	$file_names = array(
		 '/modules/quickform/templates/de/contactform (HTML5).lte'		// official distributed frontend templates, will be replaced with new ones
		,'/modules/quickform/templates/de/contactform_(HTML5).lte'
		,'/modules/quickform/templates/de/mini_contactform (HTML5).lte'
		,'/modules/quickform/templates/de/mini_contactform_(HTML5).lte'
		,'/modules/quickform/templates/de/mini_contactform_semantic (HTML5).lte'
		,'/modules/quickform/templates/de/mini_contactform_semantic_(HTML5).lte'
		,'/modules/quickform/templates/en/callme.lte'
		,'/modules/quickform/templates/en/callme (HTML5).lte'
		,'/modules/quickform/templates/en/callme_(HTML5).lte'
		,'/modules/quickform/templates/en/contactform.lte'
		,'/modules/quickform/templates/en/contactform (HTML5).lte'
		,'/modules/quickform/templates/en/contactform_(HTML5).lte'
		,'/modules/quickform/templates/en/mini_contactform.lte'
		,'/modules/quickform/templates/en/mini_contactform (HTML5).lte'
		,'/modules/quickform/templates/en/mini_contactform_(HTML5).lte'
		,'/modules/quickform/templates/en/full_contactform.lte'
		,'/modules/quickform/templates/en/full_contactform (HTML5).lte'
		,'/modules/quickform/templates/en/full_contactform_(HTML5).lte'
		,'/modules/quickform/templates/en/job_application_with_resume_upload.lte'
		,'/modules/quickform/templates/nl/bel_me_terug.lte'
		,'/modules/quickform/templates/nl/bel_me_terug (HTML5).lte'
		,'/modules/quickform/templates/nl/bel_me_terug_(HTML5).lte'
		,'/modules/quickform/templates/nl/contactformulier.lte'
		,'/modules/quickform/templates/nl/contactformulier (HTML5).lte'
		,'/modules/quickform/templates/nl/contactformulier_(HTML5).lte'
		,'/modules/quickform/templates/nl/mini_contactformulier.lte'
		,'/modules/quickform/templates/nl/mini_contactformulier (HTML5).lte'
		,'/modules/quickform/templates/nl/mini_contactformulier_(HTML5).lte'
		,'/modules/quickform/templates/nl/uitgebreid_contactformulier.lte'
		,'/modules/quickform/templates/nl/uitgebreid_contactformulier (HTML5).lte'
		,'/modules/quickform/templates/nl/uitgebreid_contactformulier_(HTML5).lte'
		,'/modules/quickform/templates/nl/uitgebreid_contactformulier_(HTML5).lte'
		,'/modules/quickform/templates/backend/email.lte'
	);
}
$file_names[] = '/modules/quickform/register_language.php';		// old function in previous LEPTON versions
$file_names[] = '/modules/quickform/register_parser.php';		// old function in previous LEPTON versions
$file_names[] = '/modules/quickform/classes/class.qform.php';	// old function in previous LEPTON versions

LEPTON_handle::delete_obsolete_files($file_names);

// delete obsolete directories if old quickform version
if ( false == file_exists( LEPTON_PATH . '/modules/quickform/templates/backend/qf_email.lte' ) )
{
		LEPTON_handle::delete_obsolete_directories('/templates/lepsem/backend/quickform');
}


// -------------------------------------------
// delete obsolete tables (since 3.1.0, wrong name)
LEPTON_handle::drop_table('xsik_quickform');
LEPTON_handle::drop_table('xsik_quickform_data');

?>
