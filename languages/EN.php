<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		Ruud Eisinga, LEPTON project
 *	@copyright		2012-2020 Ruud Eisinga, LEPTON project
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$root = '../';
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= '../';
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php 

$MOD_QUICKFORM = array(
	 "QUICKFORM"		=> "QuickForm"
	,"SETTINGS"			=> "Settings"
	,"SUBJECT"			=> "Form sent by the website"

	,"EDIT_TEMPLATE"	=> "Modify template"
	,"INFO"				=> "Modul Info"

	,"TEXT_FORM"		=> "Select frontend form"
	,"TEXT_EMAIL"		=> "Email receiver"
	,"TEXT_SUBJECT"		=> "Email Subject"
	,"TEXT_SUCCESS"		=> "Success page"
	,"TEXT_NOPAGE"		=> "No successpage, standard text only"

	,"TT_HIDE"			=> "Close the message group"
	,"ASK_DELETEMSG"	=> "Delete this message(s) ?"
	,"TT_DELETEMSG"		=> "Delete this message"
	,"TT_DELETEMSG_ALL"	=> "Delete all messages shown"
	,"TT_MSGMOVE"		=> "Move this message"
	,"TT_MSGMOVE_ALL"	=> "Move all messages shown"
	,"TT_MOVE2GROUP"	=> "Move into this message group"
	,"TT_SHOWMAIL"		=> "Show/hide message sent"
	,"TT_SHOWMAIL_ALL"	=> "Show/hide all messages sent"
	,"TT_SETROWS"		=> "Set new number of items to be listed"
	,"TT_VIEW_CLASSIC"	=> "Change to mail view"
	,"TT_VIEW_TABLE"	=> "Change to table view"
	,"TT_VIEW_SERVER"	=> "Change to bubbles view"

	,"RECEIVED"			=> "Received messages"
	,"NBRRECEIVED"		=> "newest"
	,"NBRTOTAL"			=> "of"
	,"COL_MSGID"		=> "ID"
	,"COL_DATA"			=> "Data"

	,"ASK_ADDGROUP"		=> "Define the new message group:"
	,"TT_ADDGROUP"		=> "Create a new message group"

	,"SAVEAS"			=> "Save template as"

	,"SPAMCHECK"		=> "Spam Check Settings"
	,"SPAM_INTRO"		=> "Attention! These settings are no guarantee that no more spam will be received."
	,"USE_HONEYPOT"		=> "Use Honeypot Functionality"
	,"SPAM_LOGGING"		=> "Spam Logging in TEMP table in the database"
	,"SPAM_CHECKTIME"	=> "Minimum number of seconds (0 - 60) for form data entry"
	,"SPAM_HONEYPOT"	=> "HoneyPot field / comma separated field list"
	,"SPAM_FAILPAGE"	=> "Action on error"
	,"TEXT_FAILMSG"		=> "No separate error page, display standard text"
	,"SPAMMER_FINAL"	=> "Oops, unfortunately an error has occurred. Please try again later."

	,"E_MAIL_HEADER"	=> "Message from Website"

	,"SUCCESS_THANKYOU"		=> "Thank you for filling out this form.<br />We will contact you ASAP"
	,"ERROR_TEMPLATE_MISSED"=> "A contact form will appear here soon.<br />Please try again later"
	,"ERROR_GENERIC"		=> "An error has occurred.<br />Please try again later or contact the webmaster.<br /><br />Thank you for your understanding."
	,"ERROR_REQUIRED_EMPTY"	=> "Please fill all required fields."
	,"ERROR_WRITE_DB"		=> "An internal error has occurred.<br />Please try again later."
	,"ERROR_SENDER_MISSED"	=> "An error has occurred (sender is missing).<br />Please try again later."
	,"ERROR_RECEIVER_MISSED"=> "An error has occurred (recipient missing).<br />Please define the recipient."
	,"ERROR_MAIL_MISSED"	=> "An error has occurred (mail missing).<br />Please try again later."
	,"ERROR_SENDMAIL"		=> "An error occurred during mail dispatch.<br />Please try again later."
	,"QUICKFORM_TEMPLATE"	=> "This template is a Quickform Standard Template.<br />Please save it under a different name, as your changes will be overwritten with the next release."
);

?>
