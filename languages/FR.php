<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		Ruud Eisinga, LEPTON project
 *	@copyright		2012-2020 Ruud Eisinga, LEPTON project
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 * Version FR elarifr accedinfo.com & deep.com
 */


// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$root = "../";
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= "../";
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php 

$MOD_QUICKFORM = array(
	 "QUICKFORM"		=> "QuickForm"
	,"SETTINGS"			=> "Param&eacute;tres"
	,"SUBJECT"			=> "Formulaire envoyé par le site"

	,"EDIT_TEMPLATE"	=> "Modifiez le modèle"
	,"INFO"				=> "Modul Info"

	,"TEXT_FORM"		=> "Sélectionner la forme frontale"
	,"TEXT_EMAIL"		=> "Récepteur de l&apos;email"
	,"TEXT_SUBJECT"		=> "Sujet de l&apos;email"
	,"TEXT_SUCCESS"		=> "Page de succès"
	,"TEXT_NOPAGE"		=> "Pas de successspage, texte standard uniquement"

	,"TT_HIDE"			=> "Fermer le groupe de messages"
	,"ASK_DELETEMSG"	=> "Supprimer ce message(s)"
	,"TT_DELETEMSG"		=> "Supprimer ce message"
	,"TT_DELETEMSG_ALL"	=> "Effacer tous les messages affichés"
	,"TT_MSGMOVE"		=> "Déplacer ce message"
	,"TT_MSGMOVE_ALL"	=> "Déplacer tous les messages affichés"
	,"TT_MOVE2GROUP"	=> "Passez à ce groupe de messages"
	,"TT_SHOWMAIL"		=> "Afficher/masquer le message envoyé"
	,"TT_SHOWMAIL_ALL"	=> "Afficher/masquer tous les messages envoyés"
	,"TT_SETROWS"		=> "Définir le nouveau nombre d'éléments à lister"
	,"TT_VIEW_CLASSIC"	=> "Changer la vue du courrier"
	,"TT_VIEW_TABLE"	=> "Changer la vue de la table"
	,"TT_VIEW_SERVER"	=> "Changer l&apos;affichage des bulles"

	,"RECEIVED"			=> "Messages reçus"
	,"NBRRECEIVED"		=> "le plus récent"
	,"NBRTOTAL"			=> "de"
	,"COL_MSGID"		=> "ID"
	,"COL_DATA"			=> "Données"

	,"ASK_ADDGROUP"		=> "Définir le nouveau groupe de messages:"
	,"TT_ADDGROUP"		=> "Créer un nouveau groupe de messages"

	,"SAVEAS"			=> "enregistrer le modèle sous"

	,"SPAMCHECK"		=> "Paramètres de vérification du spam"
	,"SPAM_INTRO"		=> "Attention ! Ces paramètres ne garantissent pas qu'il n'y aura plus de spam."
	,"USE_HONEYPOT"		=> "Utiliser la fonctionnalité du pot de miel"
	,"SPAM_LOGGING"		=> "Spam Logging in TEMP table in the database"
	,"SPAM_CHECKTIME"	=> "Nombre minimum de secondes (0 - 60) pour la saisie des données du formulaire"
	,"SPAM_HONEYPOT"	=> "Champ du pot de miel / Liste des champs séparés par des virgules"
	,"SPAM_FAILPAGE"	=> "Action en cas d'erreur"
	,"TEXT_FAILMSG"		=> "Pas de page d'erreur séparée, affichage du texte standard"
	,"SPAMMER_FINAL"	=> "Oups, malheureusement une erreur s'est produite. Veuillez réessayer plus tard"

	,"E_MAIL_HEADER"	=> "Notification"

	,"SUCCESS_THANKYOU"		=> "Merci d&apos;avoir rempli ce formulaire.<br />Nous vous contacterons dès que possible"
	,"ERROR_TEMPLATE_MISSED"=> "Un formulaire de contact va bientôt apparaître ici.<br />Veuillez réessayer plus tard"
	,"ERROR_GENERIC"		=> "Une erreur s&apos;est produite.<br />Veuillez réessayer plus tard ou contacter le webmaster.<br /><br />Merci de votre compréhension"
	,"ERROR_REQUIRED_EMPTY"	=> "Veuillez remplir tous les champs obligatoires."
	,"ERROR_WRITE_DB"		=> "Une erreur interne s'est produite.<br />Veuillez réessayer plus tard."
	,"ERROR_SENDER_MISSED"	=> "Une erreur est survenue (l'expéditeur est manquant).<br />Réessayez plus tard."
	,"ERROR_RECEIVER_MISSED"=> "Une erreur est survenue (destinataire manquant).<br />Veuillez définir le destinataire."
	,"ERROR_MAIL_MISSED"	=> "Une erreur est survenue (mail manquant).<br />Réessayez plus tard."
	,"ERROR_SENDMAIL"		=> "Une erreur s'est produite pendant l'envoi du courrier.<br />Veuillez réessayer plus tard."
	,"QUICKFORM_TEMPLATE"	=> "Ce modèle est un Quickform Standard Template.<br />Veuillez le sauvegarder sous un autre nom car vos modifications seront écrasées par la prochaine version."
);

?>
