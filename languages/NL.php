<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		Ruud Eisinga, LEPTON project
 *	@copyright		2012-2020 Ruud Eisinga, LEPTON project
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$root = "../";
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= "../";
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php 

$MOD_QUICKFORM = array(
	 "QUICKFORM"		=> "QuickForm"
	,"SETTINGS"			=> "Instellingen"
	,"SUBJECT"			=> "Formulier vanuit de website"

	,"EDIT_TEMPLATE"	=> "Bewerk template"
	,"INFO"				=> "Modul Info"

	,"TEXT_FORM"		=> "Kies formulier"
	,"TEXT_EMAIL"		=> "Email ontvanger"
	,"TEXT_SUBJECT"		=> "Email Onderwerp"
	,"TEXT_SUCCESS"		=> "Succes pagina"
	,"TEXT_NOPAGE"		=> "Geen succespagina, gebruik de standaard tekst."

	,"TT_HIDE"			=> "Berichtengroep sluiten"
	,"ASK_DELETEMSG"	=> "Dit bericht(en) verwijderen"
	,"TT_DELETEMSG"		=> "Dit bericht verwijderen"
	,"TT_DELETEMSG_ALL"	=> "Alle getoonde berichten verwijderen"
	,"TT_MSGMOVE"		=> "Verplaats dit bericht"
	,"TT_MSGMOVE_ALL"	=> "Verplaats alle getoonde berichten"
	,"TT_MOVE2GROUP"	=> "Ga naar deze berichtgroep"
	,"TT_SHOWMAIL"		=> "Verzonden bericht tonen/verbergen"
	,"TT_SHOWMAIL_ALL"	=> "Toon/verberg alle verzonden berichten"
	,"TT_SETROWS"		=> "Nieuw in te voeren aantal items"
	,"TT_VIEW_CLASSIC"	=> "Toon berichten als mail overzicht"
	,"TT_VIEW_TABLE"	=> "Toon berichten als tabel"
	,"TT_VIEW_SERVER"	=> "Toon berichten als bubbels"

	,"RECEIVED"			=> "Inzendingen"
	,"NBRRECEIVED"		=> "Laatste"
	,"NBRTOTAL"			=> "of"
	,"COL_MSGID"		=> "ID"
	,"COL_DATA"			=> "Data"

	,"ASK_ADDGROUP"		=> "Definieer de nieuwe berichtgroep:"
	,"TT_ADDGROUP"		=> "Creëer een nieuwe berichtgroep"

	,"SAVEAS"			=> "Sla template op als"

	,"SPAMCHECK"		=> "Spam Check Settings"
	,"SPAM_INTRO"		=> "Attentie! Deze instellingen zijn geen garantie dat er geen spam meer wordt ontvangen."
	,"USE_HONEYPOT"		=> "Honeypotfunctionaliteit gebruiken"
	,"SPAM_LOGGING"		=> "Spam Logging in TEMP-tabel in de database"
	,"SPAM_CHECKTIME"	=> "Minimum aantal seconden (0 - 60) voor het invoeren van formuliergegevens"
	,"SPAM_HONEYPOT"	=> "HoneyPot veld / Comma separated field list"
	,"SPAM_FAILPAGE"	=> "Foutieve actie"
	,"TEXT_FAILMSG"		=> "Geen aparte foutpagina, standaardtekst weergeven"
	,"SPAMMER_FINAL"	=> "Oeps, helaas is er een fout opgetreden. Probeer het later nog eens."
	,"E_MAIL_HEADER"	=> "Bericht"
	
	,"SUCCESS_THANKYOU"		=> "Bedankt voor uw reaktie.<br />We zullen zo spoedig mogelijk contact met u opnemen."
	,"ERROR_TEMPLATE_MISSED"=> "Een contactformulier zal hier binnenkort verschijnen.<br />Probeer het later nog eens."
	,"ERROR_GENERIC"		=> "Er is een fout opgetreden.<br />Probeer het later nog eens of neem contact op met de webmaster.<br /><br />Bedankt u voor uw begrip."
	,"ERROR_REQUIRED_EMPTY"	=> "Vul a.u.b. de verplichte velden in."
	,"ERROR_WRITE_DB"		=> "Er is een interne fout opgetreden.<br /> Probeer het later nog eens."
	,"ERROR_SENDER_MISSED"	=> "Er is een fout opgetreden (afzender ontbreekt)."
	,"ERROR_RECEIVER_MISSED"=> "Er is een fout opgetreden (ontvanger ontbreekt)<br />Beschrijf de ontvanger."
	,"ERROR_MAIL_MISSED"	=> "Er is een fout opgetreden (e-mail ontbreekt)."
	,"ERROR_SENDMAIL"		=> "Er is een fout opgetreden tijdens de verzending van de post.<br /> Probeer het later nog eens."
	,"QUICKFORM_TEMPLATE"	=> "Dit sjabloon is een Quickform Standard Template.<br />Sla het op onder een andere naam op, want uw wijzigingen worden bij de volgende versie overschreven."
);

?>
