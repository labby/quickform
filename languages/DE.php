<?php

/**
 *
 *	@module			quickform
 *	@version		see info.php of this module
 *	@authors		Ruud Eisinga, LEPTON project
 *	@copyright		2012-2020 Ruud Eisinga, LEPTON project
 *	@license		GNU General Public License
 *	@license terms	see info.php of this module
 *	@platform		see info.php of this module
 *
 *
 */


// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {
	include(LEPTON_PATH.'/framework/class.secure.php');
} else {
	$root = "../";
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= "../";
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) {
		include($root.'/framework/class.secure.php');
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php 

$MOD_QUICKFORM = array(
	 "QUICKFORM"		=> "QuickForm"
	,"SETTINGS"			=> "Einstellungen"
	,"SUBJECT"			=> "Formular &uuml;ber die Website verschickt"

	,"EDIT_TEMPLATE"	=> "Template anpassen"
	,"INFO"				=> "Modul Info"

	,"TEXT_FORM"		=> "Frontend Formular ausw&auml;hlen"
	,"TEXT_EMAIL"		=> "E-Mail-Empf&auml;nger"
	,"TEXT_SUBJECT"		=> "E-Mail-Betreff"
	,"TEXT_SUCCESS"		=> "Erfolgreich-Seite"
	,"TEXT_NOPAGE"		=> "Keine separate Erfolgreich-Seite, Standardtext anzeigen"
	
	,"TT_HIDE"			=> "Schließe die Nachrichtengruppe"
	,"ASK_DELETEMSG"	=> "Lösche diese Nachricht(en)?"
	,"TT_DELETEMSG"		=> "Lösche diese Nachricht"
	,"TT_DELETEMSG_ALL"	=> "Lösche alle angezeigten Nachrichten"
	,"TT_MSGMOVE"		=> "Verschiebe diese Nachricht"
	,"TT_MSGMOVE_ALL"	=> "Verschiebe alle angezeigten Nachrichten"
	,"TT_MOVE2GROUP"	=> "Verschiebe in diese Nachrichtengruppe"
	,"TT_SHOWMAIL"		=> "Zeige/Schließe versendete Nachricht"
	,"TT_SHOWMAIL_ALL"	=> "Zeige/Schließe alle versendeten Nachrichten"
	,"TT_SETROWS"		=> "Bestimme die Anzahl der anzuzeigenden Nachrichten"
	,"TT_VIEW_CLASSIC"	=> "Zeige Nachrichten in einer Mail Übersicht"
	,"TT_VIEW_TABLE"	=> "Zeige Nachrichten in Tabellenform"
	,"TT_VIEW_SERVER"	=> "Zeige Nachrichten als Bubbles"

	,"RECEIVED"			=> "Empfangene Nachrichten"
	,"NBRRECEIVED"		=> "neueste"
	,"NBRTOTAL"			=> "von"
	,"COL_MSGID"		=> "ID"
	,"COL_DATA"			=> "Daten"

	,"ASK_ADDGROUP"		=> "Neue Nachrichtengruppe eingeben:"
	,"TT_ADDGROUP"		=> "Neue Nachrichtengruppe erstellen"

	,"SAVEAS"			=> "Vorlage speichern als"

	,"SPAMCHECK"		=> "Spam Check Einstellungen"
	,"SPAM_INTRO"		=> "Achtung! Diese Einstellungen sind keine Garantie dafür, dass kein Spam mehr empfangen wird."
	,"USE_HONEYPOT"		=> "Verwende Honeypot Funktionalität"
	,"SPAM_LOGGING"		=> "Spam Logging in der TEMP Tabelle in der Datenbank"
	,"SPAM_CHECKTIME"	=> "Mindest-Anzahl Sekunden (0 - 60) für Formular-Dateneingabe"
	,"SPAM_HONEYPOT"	=> "HoneyPot Feld/mit Komma getrennte Feldliste"
	,"SPAM_FAILPAGE"	=> "Aktion bei Fehler"
	,"TEXT_FAILMSG"		=> "Keine separate Fehler-Seite, Standardtext anzeigen"
	,"SPAMMER_FINAL"	=> "Oops, leider ist ein Fehler aufgetreten. Bitte versuchen Sie es später noch einmal."

	,"E_MAIL_HEADER"	=> "Mitteilung"

	,"SUCCESS_THANKYOU"		=> "Vielen Dank f&uuml;r das Ausf&uuml;llen des Formulars.<br />Wir werden Sie schnellstm&ouml;glich kontaktieren."
	,"ERROR_TEMPLATE_MISSED"=> "Hier erscheint demnächst ein Kontaktformular.<br />Bitte versuchen Sie es später noch einmal."
	,"ERROR_GENERIC"		=> "Es ist ein Fehler aufgetreten.<br />Bitte versuchen Sie es später noch einmal oder kontaktieren den Webmaster.<br /><br />Danke für Ihr Verständniss."
	,"ERROR_REQUIRED_EMPTY"	=> "Bitte f&uuml;llen Sie alle erfolderlichen Felder aus."
	,"ERROR_WRITE_DB"		=> "Es ist ein interner Fehler aufgetreten.<br />Bitte versuchen Sie es später noch einmal."
	,"ERROR_SENDER_MISSED"	=> "Es ist ein Fehler aufgetreten (Absender fehlt).<br />Bitte versuchen Sie es später noch einmal."
	,"ERROR_RECEIVER_MISSED"=> "Es ist ein Fehler aufgetreten (Empfänger fehlt).<br />Bitte definieren Sie den Empfänger."
	,"ERROR_MAIL_MISSED"	=> "Es ist ein Fehler aufgetreten (Mail fehlt).<br />Bitte versuchen Sie es später noch einmal."
	,"ERROR_SENDMAIL"		=> "Es ist ein Fehler beim Mailversand aufgetreten.<br />Bitte versuchen Sie es später noch einmal."
	,"QUICKFORM_TEMPLATE"	=> "Dieses Template ist ein Quickform Standard-Template.<br />Bitte speichern Sie es unter einem anderen Namen, da Ihre Änderungen beim nächsten Update überschrieben werden."
);

?>
